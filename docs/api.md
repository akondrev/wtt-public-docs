# Public API (beta)

<div class="alert alert-primary" role="alert">
API_URL: https://wtt.orbl.io/api
</div>

<div class="alert alert-warning" role="alert">
Важно! Все запросы к API, кроме эндпойнта `/tokens`, должны содержать заголовок с валидным токеном.
</div>

## Получение токена

`POST API_URL/tokens` — авторизует пользователя и возвращает новый токен.

Параметры запроса (JSON):

| Parameter | Type   | Required | Default | Notes               |
|-----------|--------|:--------:|:-------:|---------------------|
| username  | string |   yes    |         | the user's name     |
| password  | string |   yes    |         | the user's password |

Пример запроса:

```sh
curl --request POST \
  --url https://wtt.orbl.io/api/tokens \
  --header 'content-type: application/json' \
  --data '{
    "username": "USERNAME",
    "password": "PASSWORD"
  }'
```

Пример ответа:

```js
200 OK
{
  "token": "a3c40...bd8c7"
}
```

## Отзыв токена

`DELETE API_URL/tokens/:token` — отзывает указанный токен.

Пример запроса:

```sh
curl --request DELETE \
  --url https://wtt.orbl.io/api/tokens/a3c40...bd8c7
```

Пример ответа:

```js
204 No Content
```

## Получение ворклогов

`GET API_URL/worklogs` — возвращает ворклоги.

Параметры запроса (Query):

| Parameter | Type     | Required | Default | Notes                                                     |
|-----------|----------|:--------:|:-------:|-----------------------------------------------------------|
| no        | integer  |          |         | только для указанного табельного номера                   |
| objects   | string   |          |         | только для указанного списка объектов (через запятую)     |
| labels    | string   |          |         | только для объектов с указанными ярлыками (через запятую) |
| from      | ISO 8601 |          |         | начиная с указанного времени                              |
| to        | ISO 8601 |          |         | по указанное время                                        |
| id_from   | integer  |          |    0    | начать с указанного идентификатора                        |
| limit     | integer  |          |   20    | ограничить выдачу                                         |
| offset    | integer  |          |    0    | пагинация                                                 |

Пример запроса:

```sh
curl --request GET \
  --url 'https://wtt.orbl.io/api/worklogs?no=012345&limit=100' \
  --header 'authorization: Bearer a3c40...bd8c7'
```

Пример ответа:

```js
200 OK
{
  "count": 42,           // всего записей в выборке (без учета пагинации)
  "prev": "https://...", // ссылка на предыдущую страницу пагинации
  "next": "https://...", // ссылка на следующую страницу пагинации
  "results": [           // массив результатов:
    {
      "id": 1,                                    // идентификатор записи
      "det_photo": "https://...jpg",              // лицо в момент идентификации
      "confidence": "exact",                      // уверенность идентификации
      "created_at": "2020-03-18T11:01:40.394Z",   // время создания записи
      "performed_at": "2020-03-18T11:01:40.394Z", // время обновления тега
      "tag": "start",                             // имя тега (start, end, etc.)
      "is_offline": false,                        // признак оффлайн-режима
      "is_offhand": false,                        // признак offhand-режима
      "device": {                                 // устройство, с которого была сделана отметка:
        "id": 1                                   //   идентификатор устройства
      },
      "location": {                               // координаты места отметки:
        "lat": 123.123,                           //   широта
        "lon": 321.321,                           //   долгота
        "deviation": 100,                         //   точность полученных координат, м
        "address": "...address",                  //   адрес
        "place": {                                //   объект по месту отметки:
          "id": 1,                                //     идентификатор объекта
          "title": "Place 1"                      //     название объекта
        },
      },
      "employee": {                               // информация о сотруднике
        "id": 1,                                  //   идентификатор сотрудника
        "no": "Employee 1",                       //   табельный номер
        "init_photo": "https://...jpg",           //   исходное фото сотрудника
        "created_at": "2020-03-18T11:01:40.394Z", //   дата создания сотрудника
        "assigned_place": {                       //   объект привязки сотрудника на момент отметки:
          "id": 2,                                //     код объекта
          "title": "Place 2"                      //     название объекта
        }
      },
      "schedule": {                               // параметры расписания на момент отметки:
        "type": "regular",                        //   тип календаря
        "start_work": "2020-03-18T11:01:40.394Z", //   начало смены
        "shift_length": 480,                      //   длительность смены, мин
        "break_duration": 60                      //   длительность перерыва, мин
      },
      "warnings": [ 'spoofing', 'out-of-object' ] // предупреждения, см. ниже
    }
  ]
}
```

| Предупреждение            | Описание                                         |
|---------------------------|--------------------------------------------------|
| early-coming              | ранний приход                                    |
| early-leaving             | ранний уход                                      |
| late-coming               | опоздание                                        |
| late-leaving              | поздний уход                                     |
| missing-gps               | отсутствует геометка                             |
| offline                   | отметка в оффлайне                               |
| out-of-assigned-object    | отметка не на своем объекте                      |
| out-of-object             | отметка не на объекте                            |
| overworked                | длинная смена                                    |
| short-shift               | короткая смена                                   |
| spoofing                  | подозрение на попытку спуфинга                   |
| timezone-detection-failed | проблема с определением времени (часового пояса) |
| weak-confidence           | низкая уверенность идентификации                 |
